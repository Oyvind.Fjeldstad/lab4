package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain implements CellAutomaton {

	IGrid currentGeneration;


	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
            for (int column = 0; column < numberOfColumns(); column++) {
                nextGeneration.set(row, column, getNextCell(row, column));
            }
        }
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		
        CellState currentState = currentGeneration.get(row, col);
		int living_neighbors = countNeighbors(row, col, CellState.ALIVE);

		if (currentState == CellState.ALIVE) {
			return CellState.DYING;
		} 
        else if (currentState == CellState.DYING) {
			return CellState.DEAD;
		} 
        else if (currentState == CellState.DEAD && living_neighbors == 2) {
            return CellState.ALIVE;
        }
        else {
			return CellState.DEAD;
		}
	}

	
	public int countNeighbors(int row, int col, CellState state) {

		if (row < 0 || row >= numberOfRows() || col < 0 || col >= numberOfColumns()) {
			throw new IndexOutOfBoundsException("Nei nei nei, index out of gridrange");
		}

		int counter=0;		
		for (int i = row-1; i < row + 2; i++){
			for (int n = col-1; n < col + 2; n++){				
				if(i<0 || n<0 ){
					continue;
				}
				else if (i >= numberOfRows() || n >= numberOfColumns()){
					continue;
				}
				else if (i == row && n == col){
					continue;
				}
				else if (getCellState(i,n) == state){				
					counter++;				
				}
			}
		}
		return counter;

	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}

