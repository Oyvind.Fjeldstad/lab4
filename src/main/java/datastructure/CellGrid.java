package datastructure;

import cellular.CellState;
import java.lang.IndexOutOfBoundsException;

public class CellGrid implements IGrid {

    int cols;
    int rows;
    CellState[][] cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.cols = columns;
        this.rows = rows;
        this.cellGrid = new CellState [rows] [columns];
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row > this.rows || column > this.cols || row < 0 || column < 0) {
            throw new IndexOutOfBoundsException("nei nei nei, index out of range");
        } 
        cellGrid[row][column] = element;    
    }

    @Override
    public CellState get(int row, int column) {
        if (row > this.rows || column > this.cols || row < 0 || column < 0) {
            throw new IndexOutOfBoundsException("nei nei nei, index out of range");
        }
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.ALIVE);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < cols; column++) {
                newGrid.set(row, column, this.get(row, column));
            }
        }
        return newGrid;

    }
    
}
